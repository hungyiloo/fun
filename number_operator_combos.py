from operator import add, sub, mul, div
from itertools import combinations_with_replacement, permutations
from inspect import isroutine
from sys import argv

def evaluate_postfix(expr):
    """ Evaluate a postfix notation and return the result """
    stack = list()
    rev_expr = list(reversed(expr))
    while len(rev_expr) > 0:
        el = rev_expr.pop()
        if isinstance(el, float):
            stack.append(el)
        else:
            num1 = stack.pop()
            num2 = stack.pop()
            stack.append(el(num1, num2))
    assert len(stack) == 1
    return stack.pop()

def is_valid_postfix(expr):
    """ Check if an expression is valid postfix notation """
    # Not used in this implementation, but it's a better way to
    # validate a postfix expression rather than ignore the exceptions.
    if isroutine(expr[0]) or isroutine(expr[1]):
        return False
    fcount = 0
    numcount = 0
    for x in expr:
        if isroutine(x):
            fcount += 1
        elif isinstance(x, float):
            numcount += 1
        if fcount >= numcount:
            return False
    return True

def get_possible_results(operators, numbers):
    """
    Give a set of operators to use, and a list of numbers,
    generate all the possible integer results by evaluating expressions
    containing each number only once and the operators, in any permutation.
    
    e.g. [1,2,3,4] AND [+,-,*,/]
         ...would yield 1+2-3*4 = -9 as one of the possibilities.
         Another possibility would be (1+2*4)/3 = 3
    
    Return a dict where the key is the possible result and the value is the
    expression used to generate that result.
    """
    result = dict()
    for opcombo in combinations_with_replacement(operators, len(numbers)-1):
        expr = list()
        expr.extend(numbers)
        expr.extend(opcombo)
        for x in permutations(expr):
            try:
                evaluation = evaluate_postfix(x)
                if evaluation % 1 == 0:
                    result[int(evaluation)] = x
            except (ZeroDivisionError, IndexError):
                pass
    return result
    
def main():
    # Use command line arguments if there are any,
    # otherwise default to a test list.
    if len(argv) > 1:
        numlist = argv[1:]
    else:
        numlist = [1, 13, 22, 71]
    # convert the numbers to floats so we will catch all possible numbers    
    numlist = map(float, numlist)
    operators = [add, sub, div, mul]
    results = get_possible_results(operators, numlist)
    for r in sorted(results):
        print r, results[r]
    exit(0)

if __name__ == '__main__':
    main()