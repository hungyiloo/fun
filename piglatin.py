import sys, re

def piglatin(word):
    vowels = 'aoeuiAOEUI' 
    # if the word begins with a vowel, fabricate a 'w' at the beginning
    if word[0] in vowels:
        word = 'w' + word
    match = re.search('[%s]' % vowels, word)
    if match is None:
        match = re.search('[yY]', word)
    if match is None:
        return word # can't translate to pig latin
    else:
        return word[match.start():] + word[:match.start()] + 'ay'

def main():
    words = sys.argv[1:]
    def pig_transform(word):
        # find trailing punctuation, and ignore it
        punctuation = ''
        match = re.search('[.,?!]+', word)
        if match is not None:
            word, punctuation = word[:match.start()], word[match.start():]
        pig = piglatin(word.lower())
        # capitalize if originally capitalized
        if word[:1].isupper():
            pig = pig.capitalize()
        return pig + punctuation
    words = map(pig_transform, words)
    print ' '.join(words)

if __name__ == '__main__':
    main()
